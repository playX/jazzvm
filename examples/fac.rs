#![feature(nll)]
#![feature(box_patterns)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
extern crate jazz_vm;
use jazz_vm::*;
fn main() {
    let mut machine = Machine::new();

    let fac = Function::from_basic_blocks(
        vec![
            BasicBlock {
                code: vec![
                    Opcode::GetLocal("n".into()),
                    Opcode::LoadFloat(1.),
                    Opcode::Eq,
                    Opcode::BasicBlockIfTrue(1),
                    Opcode::BasicBlockGoto(2),
                ],

                pc: 0,
            },
            BasicBlock {
                code: vec![Opcode::LoadLong(1), Opcode::Return],

                pc: 0,
            },
            BasicBlock {
                code: vec![
                    Opcode::GetLocal("n".into()),
                    Opcode::LoadFloat(1.),
                    Opcode::Sub,
                    Opcode::LoadNull,
                    Opcode::GetGlobal("fac".into()),
                    Opcode::Call(1),
                    Opcode::GetLocal("n".into()),
                    Opcode::Mul,
                    Opcode::Return,
                ],

                pc: 0,
            },
        ],
        vec!["n".into()],
    );

    let fac = machine.object_pool.allocate(Box::new(fac));
    let fac = Value::Object(fac);
    machine.object_pool.set_global_object("fac", fac);

    let main = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::LoadDouble(6.0),
                Opcode::LoadNull,
                Opcode::GetGlobal("fac".into()),
                Opcode::Call(1),
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec![],
    );

    let main_id = machine.object_pool.allocate(Box::new(main));
    let main_call = Value::Object(main_id);

    machine.invoke(main_call, Value::Null, None, &[]);
    machine.gc();

    let float =
        ValueContext::new(&machine.last_frame_mut().pop(), &machine.object_pool).as_object_direct();

    println!("{:?}", float.to_f64());
}
