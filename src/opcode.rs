use super::Value;

#[derive(Clone, Debug)]
pub enum Opcode {
    LoadInt(i32),
    LoadLong(i64),
    LoadFloat(f32),
    LoadDouble(f64),
    LoadString(String),
    LoadBool(bool),
    LoadNull,
    LoadThis,
    LoadObject(usize),
    SetThis,
    Call(usize),
    CallField(usize),

    GetField(String),
    SetField(String),

    GetArgument(usize),
    GetNArguments,

    GetGlobal(String),
    SetGlobal(String),

    GetLocal(String),
    SetLocal(String),
    Dup,
    DupX2,
    Neg,
    Return,

    Add,
    Sub,
    Div,
    Mul,
    Mod,
    Pow,
    Shr,
    Shl,

    GotoIfTrue(usize),
    GotoIfFalse(usize),
    Goto(usize),
    BasicBlockGoto(usize),
    BasicBlockIfTrue(usize),
    BasicBlockIfFalse(usize),

    Eq,
    Gt,
    Ge,
    Lt,
    Le,
    Not,

    Reverse(usize),

    BitAnd,
    BitOr,
    BitXor,
    And,
    Or,
}

#[derive(Debug, Clone)]
pub enum RegOpcode {
    Mov(usize, Value),
    IntAdd(usize, usize),
    IntSub(usize, usize),
    IntDiv(usize, usize),
    IntMul(usize, usize),
    FloatAdd(usize, usize),
    FloatSub(usize, usize),
    FloatDiv(usize, usize),
    FloatMul(usize, usize),
}

#[derive(Clone, Debug, PartialEq)]
pub enum ValueLocation {
    Stack(isize),
    Local(usize),
    Argument(usize),
    ConstInt(i64),
    ConstFloat(f64),
    ConstString(String),
    ConstBool(bool),
    ConstNull,
    ConstObject(usize),
    This,
}
