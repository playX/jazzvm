use super::machine::Machine;
use super::object::Object;
use super::object::ObjectPool;
use super::value::Value;
use super::value::*;
use std::any::Any;
use std::cmp::Ordering;

impl Object for bool {
    fn as_any(&self) -> &Any {self as &Any}
    fn as_any_mut(&mut self) -> &mut Any {self as &mut Any}

    fn initialize(&mut self, _pool: &mut ObjectPool) {}
    fn get_children(&self) -> Vec<usize> {
        vec![]
    }

    fn o_clone(&self, p: &mut ObjectPool) -> Value {
        let v = p.allocate(Box::new(self.clone()));
        Value::Object(v)
    }

    fn to_i64(&self) -> i64 {
        if *self {
            return 1;
        } else {
            return 0;
        }
    }

    fn to_f64(&self) -> f64 {
        if *self {
            return 1.0;
        } else {
            return 0.0;
        }
    }

    fn toString(&self) -> String {
        if *self {
            return "true".into()
        } else {
            return "false".into()
        }
    }

    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        match field_name {
            "toString" => {
                let str = self.toString();

                let allocated =Value::Object(executor.object_pool.allocate(Box::new(str)));
                return allocated;
            }
            _ => unimplemented!()
        }
    }
}

impl Object for String {
    fn get_children(&self) -> Vec<usize> {
        Vec::new()
    }

    fn typename(&self) -> &str {
        "string"
    }

    fn as_any(&self) -> &Any {
        self as &Any
    }
    fn o_clone(&self, p: &mut ObjectPool) -> Value {
        let v = p.allocate(Box::new(self.clone()));
        Value::Object(v)
    }
    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn to_i64(&self) -> i64 {
        match self.as_str().parse::<i64>() {
            Ok(v) => v,
            Err(_) => panic!("Cannot parse as i64"),
        }
    }

    fn to_f64(&self) -> f64 {
        match self.as_str().parse::<f64>() {
            Ok(v) => v,
            Err(_) => panic!("Cannot parse as f64"),
        }
    }

    fn to_str(&self) -> &str {
        self.as_str()
    }

    fn to_bool(&self) -> bool {
        *self == ""
    }

    fn test_eq(&self, other: &ValueContext) -> bool {
        if let Some(other) = other.as_object_direct().as_any().downcast_ref::<Self>() {
            *other == *self
        } else {
            false
        }
    }

    fn compare(&self, other: &ValueContext) -> Option<Ordering> {
        if let Some(other) = other.as_object_direct().as_any().downcast_ref::<Self>() {
            self.partial_cmp(&other)
        } else {
            None
        }
    }

    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        match field_name {
            "__add__" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() + ValueContext::new(&right, &executor.object_pool)
                    .to_str()
                    .as_ref();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "as_str" => {
                let id = executor.object_pool.allocate(Box::new(self.clone()));
                Value::Object(id)
            }
            _ => panic!("Not found {}", field_name),
        }
    }
}

impl Object for f64 {
    fn get_children(&self) -> Vec<usize> {
        Vec::new()
    }
    fn typename(&self) -> &str {
        "double"
    }

    fn as_any(&self) -> &Any {
        self as &Any
    }

    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn to_i64(&self) -> i64 {
        *self as i64
    }

    fn to_f64(&self) -> f64 {
        *self
    }
    fn o_clone(&self, p: &mut ObjectPool) -> Value {
        let v = p.allocate(Box::new(self.clone()));
        Value::Object(v)
    }
    fn to_f32(&self) -> f32 {
        *self as f32
    }
    fn to_i32(&self) -> i32 {
        *self as i32
    }

    fn to_bool(&self) -> bool {
        if *self > 0.0 {
            true
        } else {
            false
        }
    }

    fn toString(&self) -> String {
        self.to_string()
    }

    fn test_eq(&self, other: &ValueContext) -> bool {
        if let Some(other) = other.as_object_direct().as_any().downcast_ref::<Self>() {
            *other == *self
        } else {
            false
        }
    }

    fn compare(&self, other: &ValueContext) -> Option<Ordering> {
        if let Some(other) = other.as_object_direct().as_any().downcast_ref::<Self>() {
            self.partial_cmp(&other)
        } else {
            None
        }
    }

    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        match field_name {
            "__add__" | "add" | "+" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() + ValueContext::new(&right, &executor.object_pool).to_f64();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }

            "as_str" => {
                let ret = self.to_string();
                let object = Value::Object(executor.object_pool.allocate(Box::new(ret)));
                return object;
            }

            "__neg__" => {
                let ret = -self;
                let object = Value::Object(executor.object_pool.allocate(Box::new(ret)));
                return object;
            }
            "__sub__" | "sub" | "-" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() - ValueContext::new(&right, &executor.object_pool).to_f64();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__mul__" | "mul" | "*" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() * ValueContext::new(&right, &executor.object_pool).to_f64();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__div__" | "div" | "/" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() / ValueContext::new(&right, &executor.object_pool).to_f64();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__pow__" | "pow" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self
                    .clone()
                    .powf(ValueContext::new(&right, &executor.object_pool).to_f64());
                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }

            _ => panic!("Not found {}", field_name),
        }
    }
}

impl Object for i64 {
    fn get_children(&self) -> Vec<usize> {
        Vec::new()
    }
    fn typename(&self) -> &str {
        "long"
    }

    fn o_clone(&self, p: &mut ObjectPool) -> Value {
        let v = p.allocate(Box::new(self.clone()));
        Value::Object(v)
    }

    fn as_any(&self) -> &Any {
        self as &Any
    }

    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn to_i64(&self) -> i64 {
        *self as i64
    }

    fn to_f64(&self) -> f64 {
        *self as f64
    }

    fn to_i32(&self) -> i32 {
        *self as i32
    }

    fn to_f32(&self) -> f32 {
        *self as f32
    }

    fn toString(&self) -> String {
        self.to_string()
    }

    fn to_bool(&self) -> bool {
        if *self > 0 {
            true
        } else {
            false
        }
    }

    fn test_eq(&self, other: &ValueContext) -> bool {
        if let Some(other) = other.as_object_direct().as_any().downcast_ref::<Self>() {
            *other == *self
        } else {
            false
        }
    }

    fn compare(&self, other: &ValueContext) -> Option<Ordering> {
        if let Some(other) = other.as_object_direct().as_any().downcast_ref::<Self>() {
            self.partial_cmp(&other)
        } else {
            None
        }
    }

    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        match field_name {
            "__add__" | "add" | "+" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() + ValueContext::new(&right, &executor.object_pool).to_i64();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }

            "__neg__" => {
                let ret = -self;
                let object = Value::Object(executor.object_pool.allocate(Box::new(ret)));
                return object;
            }

            "as_str" => {
                let ret = self.to_string();
                let object = Value::Object(executor.object_pool.allocate(Box::new(ret)));
                return object;
            }
            "__sub__" | "sub" | "-" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() - ValueContext::new(&right, &executor.object_pool).to_i64();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__mul__" | "mul" | "*" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() * ValueContext::new(&right, &executor.object_pool).to_i64();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__div__" | "div" | "/" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() / ValueContext::new(&right, &executor.object_pool).to_i64();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__pow__" | "pow" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self
                    .clone()
                    .pow(ValueContext::new(&right, &executor.object_pool).to_i64() as u32);
                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }

            _ => panic!("Not found {}", field_name),
        }
    }
}

impl Object for i32 {
    fn get_children(&self) -> Vec<usize> {
        Vec::new()
    }
    fn typename(&self) -> &str {
        "int"
    }

    fn as_any(&self) -> &Any {
        self as &Any
    }

    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn to_i64(&self) -> i64 {
        *self as i64
    }

    fn to_f64(&self) -> f64 {
        *self as f64
    }

    fn to_f32(&self) -> f32 {
        *self as f32
    }

    fn to_i32(&self) -> i32 {
        *self
    }

    fn o_clone(&self, p: &mut ObjectPool) -> Value {
        let v = p.allocate(Box::new(self.clone()));
        Value::Object(v)
    }

    fn toString(&self) -> String {
        self.to_string()
    }

    fn to_bool(&self) -> bool {
        if *self > 0 {
            true
        } else {
            false
        }
    }

    fn test_eq(&self, other: &ValueContext) -> bool {
        if let Some(other) = other.as_object_direct().as_any().downcast_ref::<Self>() {
            *other == *self
        } else {
            false
        }
    }

    fn compare(&self, other: &ValueContext) -> Option<Ordering> {
        if let Some(other) = other.as_object_direct().as_any().downcast_ref::<Self>() {
            self.partial_cmp(&other)
        } else {
            None
        }
    }

    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        match field_name {
            "__neg__" => {
                let ret = -self;
                let object = Value::Object(executor.object_pool.allocate(Box::new(ret)));
                return object;
            }
            "__add__" | "add" | "+" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() + ValueContext::new(&right, &executor.object_pool).to_i32();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }

            "as_str" => {
                let ret = self.to_string();
                let object = Value::Object(executor.object_pool.allocate(Box::new(ret)));
                return object;
            }
            "__sub__" | "sub" | "-" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() - ValueContext::new(&right, &executor.object_pool).to_i32();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__mul__" | "mul" | "*" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() * ValueContext::new(&right, &executor.object_pool).to_i32();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__div__" | "div" | "/" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() / ValueContext::new(&right, &executor.object_pool).to_i32();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__pow__" | "pow" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self
                    .clone()
                    .pow(ValueContext::new(&right, &executor.object_pool).to_i64() as u32);
                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }

            _ => panic!("Not found {}", field_name),
        }
    }
}

impl Object for f32 {
    fn get_children(&self) -> Vec<usize> {
        Vec::new()
    }

    fn o_clone(&self, p: &mut ObjectPool) -> Value {
        let v = p.allocate(Box::new(self.clone()));
        Value::Object(v)
    }
    fn typename(&self) -> &str {
        "float"
    }

    fn as_any(&self) -> &Any {
        self as &Any
    }

    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn toString(&self) -> String {
        self.to_string()
    }

    fn to_i64(&self) -> i64 {
        *self as i64
    }

    fn to_f64(&self) -> f64 {
        *self as f64
    }

    fn to_f32(&self) -> f32 {
        *self as f32
    }

    fn to_i32(&self) -> i32 {
        *self as i32
    }

    fn to_bool(&self) -> bool {
        if *self > 0.0 {
            true
        } else {
            false
        }
    }

    fn test_eq(&self, other: &ValueContext) -> bool {
        if let Some(other) = other.as_object_direct().as_any().downcast_ref::<Self>() {
            *other == *self
        } else {
            false
        }
    }

    fn compare(&self, other: &ValueContext) -> Option<Ordering> {
        if let Some(other) = other.as_object_direct().as_any().downcast_ref::<Self>() {
            self.partial_cmp(&other)
        } else {
            None
        }
    }

    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        match field_name {
            "__add__" | "add" | "+" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() + ValueContext::new(&right, &executor.object_pool).to_f32();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }

            "__neg__" => {
                let ret = -self;
                let object = Value::Object(executor.object_pool.allocate(Box::new(ret)));
                return object;
            }

            "as_str" => {
                let ret = self.to_string();
                let object = Value::Object(executor.object_pool.allocate(Box::new(ret)));
                return object;
            }
            "__sub__" | "sub" | "-" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() - ValueContext::new(&right, &executor.object_pool).to_f32();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__mul__" | "mul" | "*" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() * ValueContext::new(&right, &executor.object_pool).to_f32();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__div__" | "div" | "/" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self.clone() / ValueContext::new(&right, &executor.object_pool).to_f32();

                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }
            "__pow__" | "pow" => {
                let right = executor.last_frame().must_get_argument(0);
                let ret = self
                    .clone()
                    .powf(ValueContext::new(&right, &executor.object_pool).to_f32());
                Value::Object(executor.object_pool.allocate(Box::new(ret)))
            }

            _ => panic!("Not found {}", field_name),
        }
    }
}
