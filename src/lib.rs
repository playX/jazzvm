#![feature(nll)]
#![feature(box_patterns)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused_imports)]
pub mod frame;
pub mod function;
pub mod jazz_std;
pub mod machine;
pub mod object;
pub mod opcode;
pub mod static_root;
pub mod value;

pub use self::frame::*;
pub use self::function::*;
pub use self::machine::*;
pub use self::object::*;
pub use self::opcode::*;
pub use self::static_root::*;
pub use self::value::*;
pub mod primitives;
pub use self::primitives::*;

#[macro_export]
macro_rules! alloc {
    ($vm: expr,$v: expr) => {{
        let id = $vm.object_pool.allocate(Box::new($v));
        $vm.push(Value::Object(id));
    }};
}
