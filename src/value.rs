use super::object::*;
use super::*;
use std::borrow::Cow;
use std::cmp::Ordering;

pub type Id = usize;

#[derive(Clone, Debug, Copy, PartialEq, PartialOrd)]
pub enum Value {
    Int(i64),
    Float(f64),
    Bool(bool),
    Object(Id),
    Null,
}

impl Value {
    pub fn is_object(&self) -> bool {
        if let Value::Object(_) = *self {
            true
        } else {
            false
        }
    }

    pub fn as_object_id(&self) -> usize {
        if let Value::Object(obj) = *self {
            obj
        } else {
            panic!("Not an object: {:?}", self);
        }
    }

    pub fn to_opcode(&self) -> Opcode {
        match *self {
            Value::Object(id) => Opcode::LoadObject(id),
            Value::Null => Opcode::LoadNull,
            Value::Bool(v) => Opcode::LoadBool(v),
            Value::Int(v) => Opcode::LoadLong(v),
            Value::Float(v) => Opcode::LoadDouble(v),
        }
    }
}

pub struct ValueContext<'a, 'b> {
    pub value: &'a Value,
    pub pool: &'b ObjectPool,
}

impl<'a, 'b> ValueContext<'a, 'b> {
    pub fn new(v: &'a Value, pool: &'b ObjectPool) -> ValueContext<'a, 'b> {
        ValueContext {
            value: v,
            pool: pool,
        }
    }

    pub fn is_object(&self) -> bool {
        self.value.is_object()
    }

    pub fn as_object_id(&self) -> usize {
        self.value.as_object_id()
    }

    pub fn as_object<'z>(&self) -> ObjectHandle<'z> {
        self.pool.get(self.as_object_id())
    }

    pub fn as_object_direct(&self) -> &'b Object {
        self.pool.get_direct(self.as_object_id())
    }

    pub fn to_i64(&self) -> i64 {
        match *self.value {
            Value::Object(id) => self.pool.get_direct(id).to_i64(),
            Value::Null => 0,
            Value::Bool(v) => {
                if v {
                    1
                } else {
                    0
                }
            }
            Value::Int(v) => v,
            Value::Float(v) => v as i64,
        }
    }

    pub fn to_f32(&self) -> f32 {
        match *self.value {
            Value::Object(id) => self.pool.get_direct(id).to_f32(),
            Value::Null => 0.0,
            Value::Bool(v) => {
                if v {
                    1.0
                } else {
                    0.0
                }
            }
            Value::Int(v) => v as f32,
            Value::Float(v) => v as f32,
        }
    }

    pub fn to_i32(&self) -> i32 {
        match *self.value {
            Value::Object(id) => self.pool.get_direct(id).to_i32(),
            Value::Null => 0,
            Value::Bool(v) => {
                if v {
                    1i32
                } else {
                    0i32
                }
            }
            Value::Int(v) => v as i32,
            Value::Float(v) => v as i32,
        }
    }

    pub fn to_f64(&self) -> f64 {
        match *self.value {
            Value::Object(id) => self.pool.get_direct(id).to_f64(),
            Value::Null => 0.0,
            Value::Bool(v) => {
                if v {
                    1.0
                } else {
                    0.0
                }
            }
            Value::Int(v) => v as f64,
            Value::Float(v) => v,
        }
    }

    pub fn to_bool(&self) -> bool {
        match *self.value {
            Value::Object(id) => self.pool.get_direct(id).to_bool(),
            Value::Null => false,
            Value::Bool(v) => v,
            Value::Int(v) => {
                if v != 0 {
                    true
                } else {
                    false
                }
            }
            Value::Float(v) => {
                if v != 0.0 {
                    true
                } else {
                    false
                }
            }
        }
    }

    pub fn compare(&self, other: &ValueContext) -> Option<Ordering> {
        if let Value::Object(_) = *self.value {
            return self.as_object_direct().compare(other);
        }
        if let Value::Object(_) = *other.value {
            return other.as_object_direct().compare(self);
        }

        match (*self.value, *other.value) {
            (Value::Null, Value::Null) => Some(Ordering::Equal),
            (Value::Bool(a), Value::Bool(b)) => a.partial_cmp(&b),
            (Value::Int(a), Value::Int(b)) => a.partial_cmp(&b),
            (Value::Float(a), Value::Float(b)) => a.partial_cmp(&b),
            (Value::Int(a), Value::Float(b)) => (a as f64).partial_cmp(&b),
            (Value::Float(a), Value::Int(b)) => a.partial_cmp(&(b as f64)),
            _ => None,
        }
    }

    pub fn to_str<'z>(&'z self) -> Cow<'z, str> {
        match *self.value {
            Value::Object(_) => Cow::from(self.as_object_direct().to_str()),
            Value::Null => Cow::from("(null)"),
            Value::Bool(v) => Cow::from(if v { "true" } else { "false" }),
            Value::Int(v) => Cow::from(format!("{}", v)),
            Value::Float(v) => Cow::from(format!("{}", v)),
        }
    }
}
