#![feature(nll)]
#![feature(box_patterns)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
mod frame;
pub mod jazz_std;
mod machine;
mod object;
mod opcode;
mod primitives;
mod static_root;
mod value;

mod function;
pub use self::frame::*;
pub use self::function::*;
pub use self::machine::*;
pub use self::object::*;
pub use self::opcode::*;
pub use self::static_root::*;
pub use self::value::*;
fn main() {
    let fun = Function::from_basic_blocks(
        vec![
            BasicBlock {
                code: vec![
                    Opcode::LoadInt(0),
                    Opcode::SetLocal("i".into()),
                    Opcode::BasicBlockGoto(1),
                ],
                pc: 0,
            },
            BasicBlock {
                code: vec![
                    // while i < 10000 {
                    //      goto_BasicBlock(2);
                    //}
                    Opcode::GetLocal("i".into()),
                    Opcode::LoadInt(10000),
                    Opcode::Gt,
                    Opcode::BasicBlockIfTrue(2),
                    Opcode::GetLocal("i".into()),
                    Opcode::Return,
                ],
                pc: 0,
            },
            BasicBlock {
                // i = i + 1;
                code: vec![
                    //Opcode::LoadString("as_str".into()),
                    //Opcode::LoadNull,
                    Opcode::GetLocal("i".into()),
                    Opcode::LoadInt(1),
                    Opcode::Add,
                    Opcode::SetLocal("i".into()),
                    Opcode::BasicBlockGoto(1),
                ],
                pc: 0,
            },
        ],
        vec![],
    );

    let mut machine = Machine::new();
    let fun_obj_id = machine.object_pool.allocate(Box::new(fun));
    let while_code = Value::Object(fun_obj_id);
    machine
        .object_pool
        .set_global_object("while_test", while_code);

    let fac = Function::from_basic_blocks(
        vec![
            BasicBlock {
                code: vec![
                    Opcode::GetLocal("n".into()),
                    Opcode::LoadFloat(1.),
                    Opcode::Eq,
                    Opcode::BasicBlockIfTrue(1),
                    Opcode::BasicBlockGoto(2),
                ],

                pc: 0,
            },
            BasicBlock {
                code: vec![Opcode::LoadLong(1), Opcode::Return],

                pc: 0,
            },
            BasicBlock {
                code: vec![
                    Opcode::GetLocal("n".into()),
                    Opcode::LoadFloat(1.),
                    Opcode::Sub,
                    Opcode::LoadNull,
                    Opcode::GetGlobal("fac".into()),
                    Opcode::Call(1),
                    Opcode::GetLocal("n".into()),
                    Opcode::Mul,
                    Opcode::Return,
                ],

                pc: 0,
            },
        ],
        vec!["n".into()],
    );

    let fac = machine.object_pool.allocate(Box::new(fac));
    let fac = Value::Object(fac);
    machine.object_pool.set_global_object("fac", fac);

    let main = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::LoadNull,
                Opcode::GetGlobal("while_test".into()),
                Opcode::Call(0),
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec![],
    );

    let main_id = machine.object_pool.allocate(Box::new(main));
    let main_call = Value::Object(main_id);

    machine.invoke(main_call, Value::Null, None, &[]);
    machine.gc();

    //let pool = new_m.object_pool;
    let float =
        ValueContext::new(&machine.last_frame_mut().pop(), &machine.object_pool).as_object_direct();

    println!("{:?}", float.to_f64());
}
