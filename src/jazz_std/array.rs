use super::machine::Machine;
use super::object::Object;
use super::object::ObjectPool;
use super::value::{Value, ValueContext};
use std::any::Any;
use std::cell::RefCell;

#[derive(Debug, Clone)]
pub struct Array {
    pub elements: RefCell<Vec<Value>>,
}

impl Array {
    pub fn new() -> Array {
        Array {
            elements: RefCell::new(Vec::new()),
        }
    }
}

impl Object for Array {
    fn get_children(&self) -> Vec<usize> {
        self.elements
            .borrow()
            .iter()
            .filter(|v| v.is_object())
            .map(|v| v.as_object_id())
            .collect()
    }

    fn o_clone(&self, p: &mut ObjectPool) -> Value {
        let v = p.allocate(Box::new(self.clone()));
        Value::Object(v)
    }

    fn as_any(&self) -> &Any {
        self as &Any
    }

    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn toString(&self) -> String {
        format!("Array({})", self.elements.borrow().len())
    }

    fn call_field(&self, name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        match name {
            "__get__" | "get" => {
                let index = executor.last_frame().arguments.last().unwrap();
                let index = ValueContext::new(&index, executor.get_object_pool()).to_i64() as usize;
                let elements = self.elements.borrow();
                if index >= elements.len() {
                    panic!("Array index out of bound")
                }
                elements[index]
            }
            "__set__" | "set" => {
                let len = executor.last_frame().arguments.len();
                let index = executor.last_frame().arguments.last().unwrap();
                let val = executor.last_frame().arguments[len - 1];

                let index = ValueContext::new(&index, executor.get_object_pool()).to_i64() as usize;
                let len = self.elements.borrow().len();
                if index >= len {
                    panic!("Array index out of bound")
                }
                self.elements.borrow_mut()[index] = val;

                Value::Null
            }
            "push" | "append" => {
                let val = executor.last_frame().arguments.last().unwrap();
                self.elements.borrow_mut().push(val.clone());
                Value::Null
            }
            "pop" => self
                .elements
                .borrow_mut()
                .pop()
                .unwrap_or_else(|| panic!("No elements")),
            "__len__" | "len" | "size" => Value::Int(self.elements.borrow().len() as i64),
            _ => panic!("field not found `{}`", name),
        }
    }
}
