use super::machine::Machine;
use super::object::*;
use super::value::*;

use std::any::Any;
use std::cell::RefCell;
use std::collections::HashMap;
use std::io::*;
pub struct IO {
    pub temp_vars: RefCell<HashMap<String, Value>>,
}

impl IO {
    pub fn new() -> IO {
        IO {
            temp_vars: RefCell::new(HashMap::new()),
        }
    }
}

impl Object for IO {
    fn get_children(&self) -> Vec<usize> {
        Vec::new()
    }
    fn o_clone(&self, _p: &mut ObjectPool) -> Value {
        panic!("Cannot clone")
    }
    fn as_any(&self) -> &Any {
        self as &Any
    }
    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn typename(&self) -> &str {
        "IO"
    }

    fn set_field(&self, _name: &str, _value_ref: Value) {
        let mut b = self.temp_vars.borrow_mut();
        (*b).insert(_name.into(), _value_ref);
    }

    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        match field_name {
            "writeln" => {
                let args = {
                    let mut temp = vec![];
                    let mut i = 0;
                    if _argc.is_some() {
                        while i < _argc.unwrap() {
                            let v = executor
                                .last_frame_mut()
                                .arguments
                                .pop()
                                .expect("no value to pop in args");
                            temp.push(v);
                            i += 1;
                        }
                    }
                    temp
                };
                /*if v.is_some() {
                    let pool = RefCell::new(executor.get_object_pool());
                    let val_ctx = ValueContext::new(v.unwrap(), &pool.borrow());
                    let val = val_ctx.as_object_direct().toString();
                
                    stdout
                        .write(val.as_bytes())
                        .expect("Cannot write to stdout");
                    print!("\n");
                } else {
                    print!("\n");
                }*/

                for val in args.iter() {
                    let pool = executor.get_object_pool();
                    if let Value::Bool(b) = val {
                        print!("{}", b);
                    } else {
                        let val = ValueContext::new(val, &pool).as_object();
                        let str = val.toString();
                        print!("{}", str);
                    }
                }
                println!();

                Value::Null
            }
            "write" => {
                let args = {
                    let mut temp = vec![];
                    let mut i = 0;
                    if _argc.is_some() {
                        while i < _argc.unwrap() {
                            let v = executor.last_frame_mut().arguments.pop().unwrap();
                            temp.push(v);
                            i += 1;
                        }
                    }
                    temp
                };
                for val in args.iter() {
                    let pool = executor.get_object_pool();
                    if let Value::Bool(b) = val {
                        print!("{}", b);
                    } else {
                        let val = ValueContext::new(val, &pool).as_object();
                        let str = val.toString();
                        print!("{}", str);
                    }
                }
                Value::Null
            }

            "read_line" => {
                let stdin = stdin();

                let mut buff = String::new();

                stdin.read_line(&mut buff).expect("Cannot read line");

                let val = Value::Object(executor.object_pool.allocate(Box::new(buff)));
                val
            }
            _ => panic!("Cannot find field `{}`", field_name),
        }
    }
}
