# Grammar

```
define func(arg1,arg2) {
    temp a => $arg1;
    temp b => $arg2;

    temp c = $a + $b;
    
    return $c

}
```