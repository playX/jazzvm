use super::value::Value;
use std::cell::Cell;

use super::opcode::Opcode;
use std::collections::HashMap;
#[derive(Clone, Debug)]
pub struct Frame {
    pub this: Cell<Value>,
    pub locals: HashMap<String, Value>,
    pub(crate) stack: Vec<Value>,
    pub arguments: Vec<Value>,
    pub current_basic_block_id: usize,
    pub basic_blocks: Vec<BasicBlock>,
}
#[derive(Clone, Debug)]
pub struct BasicBlock {
    pub code: Vec<Opcode>,
    pub pc: usize,
}

use std::collections::HashSet;

pub struct CallStack {
    pub frames: Vec<Frame>,
    pub n_frames: usize,
    pub limit: Option<usize>,
}

impl CallStack {
    pub fn new(len: usize) -> CallStack {
        let mut frames = Vec::with_capacity(len);
        for _ in 0..len {
            frames.push(Frame::new());
        }

        CallStack {
            frames: frames,
            n_frames: 1, // one 'initial' frame
            limit: None,
        }
    }

    pub fn set_limit(&mut self, limit: usize) {
        self.limit = Some(limit);
    }

    pub fn push(&mut self) {
        if self.n_frames >= self.frames.len() {
            panic!("Virtual stack overflow");
        }
        if let Some(limit) = self.limit {
            if self.n_frames >= limit {
                panic!("Maximum stack depth exceeded");
            }
        }
        self.n_frames += 1;
    }

    pub fn pop(&mut self) {
        if self.n_frames <= 0 {
            panic!("Virtual stack underflow");
        }
        self.frames[self.n_frames - 1].reset();
        self.n_frames -= 1;
    }

    pub fn top(&self) -> &Frame {
        if self.n_frames <= 0 {
            panic!("Virtual stack underflow");
        }
        &self.frames[self.n_frames - 1]
    }

    pub fn top_mut(&mut self) -> &mut Frame {
        if self.n_frames <= 0 {
            panic!("Virtual stack underflow");
        }
        &mut self.frames[self.n_frames - 1]
    }
    pub fn collect_objects(&self) -> Vec<usize> {
        let mut objs = HashSet::new();
        for i in 0..self.n_frames {
            let frame = &self.frames[i];
            if let Value::Object(id) = frame.this.get() {
                objs.insert(id);
            }
            for _i in 0..frame.arguments.len() {
                let v = frame.arguments.get(i).unwrap();

                if let Value::Object(id) = v {
                    objs.insert(*id);
                }
            }
            for _i in 0..frame.locals.len() {
                for (_key, val) in frame.locals.iter() {
                    if let Value::Object(id) = val {
                        objs.insert(*id);
                    }
                }
            }
            for i in 0..frame.stack.len() {
                let v = frame.stack.get(i).unwrap();
                if let Value::Object(id) = v {
                    objs.insert(*id);
                }
            }
        }
        objs.into_iter().collect()
    }
}

impl Frame {
    pub fn new() -> Frame {
        Frame {
            this: Cell::new(Value::Null),
            locals: HashMap::new(),
            stack: vec![],
            arguments: vec![],
            current_basic_block_id: 0,
            basic_blocks: vec![],
        }
    }

    pub fn fetch_opcode(&mut self) -> Opcode {
        let curr_basic_block = &self.basic_blocks[self.current_basic_block_id].clone();
        let code = &curr_basic_block.code[curr_basic_block.pc];
        self.basic_blocks[self.current_basic_block_id].pc += 1;
        code.clone()
    }

    pub fn goto(&mut self, idx: usize) {
        self.basic_blocks.last_mut().unwrap().pc = idx;
    }

    fn reset(&mut self) {
        self.this.set(Value::Null);
        self.arguments.clear();
        self.locals.clear();
        self.stack.clear();
    }

    pub fn init_with_arguments(&mut self, this: Value, args: &[Value]) {
        self.this.set(this);
        for arg in args {
            self.arguments.push(*arg);
        }
    }

    pub fn switch_basic_block(&mut self, idx: usize) {
        self.current_basic_block_id = idx;
        self.basic_blocks[self.current_basic_block_id].pc = 0;
    }

    pub fn pop(&mut self) -> Value {
        self.stack
            .pop()
            .expect(&format!("No value to pop in stack, \n{:?}\n\n", self))
    }

    pub fn push(&mut self, v: Value) {
        self.stack.push(v);
    }

    pub fn set_this(&self, t: Value) {
        self.this.set(t);
    }

    pub fn get_this(&self) -> Value {
        self.this.get()
    }

    #[inline]
    pub fn get_argument(&self, key: usize) -> Option<&Value> {
        self.arguments.get(key)
    }

    #[inline]
    pub fn must_get_argument(&self, key: usize) -> &Value {
        self.get_argument(key)
            .unwrap_or_else(|| panic!("Argument index out of bound"))
    }

    #[inline]
    pub fn get_n_arguments(&self) -> usize {
        self.arguments.len()
    }

    pub fn store_var(&mut self, key: &str, v: Value) {
        self.locals.insert(key.to_string(), v);
    }

    pub fn get_var(&mut self, key: &str) -> Value {
        *self
            .locals
            .get(key)
            .expect(&format!("Variable `{}` not found", key))
    }
}
