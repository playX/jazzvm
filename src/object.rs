use super::frame::CallStack;
use super::machine::Machine;
use super::static_root::StaticRoot;
use super::value::Value;
use super::value::*;
use std::any::Any;
use std::cmp::Ordering;
pub trait Object: Send {
    fn finalize(&self, _pool: &mut ObjectPool) {}

    fn o_clone(&self, _pool: &mut ObjectPool) -> Value;
    // before allocating on the object pool...
    fn initialize(&mut self, _pool: &mut ObjectPool) {}

    fn call(&self, _executor: &mut Machine, _argc: Option<usize>) -> Value {
        panic!("Not callable");
    }
    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        let field = self.must_get_field(executor.get_object_pool(), field_name);
        let obj = ValueContext::new(&field, executor.get_object_pool()).as_object();
        obj.call(executor, _argc)
    }
    fn get_field(&self, _pool: &ObjectPool, _name: &str) -> Option<Value> {
        None
    }
    fn set_field(&self, _name: &str, _value_ref: Value) {
        panic!("Cannot set field");
    }
    fn must_get_field(&self, pool: &ObjectPool, name: &str) -> Value {
        match self.get_field(pool, name) {
            Some(v) => v,
            None => panic!(format!("Field `{}` not found", name)),
        }
    }
    fn has_const_field(&self, _pool: &ObjectPool, _name: &str) -> bool {
        false
    }
    fn compare(&self, _other: &ValueContext) -> Option<Ordering> {
        None
    }
    fn test_eq(&self, _other: &ValueContext) -> bool {
        false
    }
    fn typename(&self) -> &str {
        "object"
    }

    fn to_f32(&self) -> f32 {
        panic!("Cannot cast to f32");
    }

    fn to_i32(&self) -> i32 {
        panic!("Cannot cast to i32");
    }

    fn to_i64(&self) -> i64 {
        panic!("Cannot cast to i64");
    }
    fn to_f64(&self) -> f64 {
        panic!("Cannot cast to f64");
    }
    fn to_str(&self) -> &str {
        panic!("Cannot cast to str, {}", self.typename());
    }
    fn toString(&self) -> String {
        self.to_str().to_string()
    }
    fn to_bool(&self) -> bool {
        panic!("Cannot cast to bool");
    }
    fn get_children(&self) -> Vec<usize>;
    fn as_any(&self) -> &Any;
    fn as_any_mut(&mut self) -> &mut Any;
}

use std::cell::Cell;
use std::ops::Deref;
use std::rc::Rc;

pub struct ObjectInfo {
    object: Box<Object>,
    native_ref_info: ObjectNativeRefInfo,
}

pub struct ObjectHandle<'a> {
    object: &'a Object,
    _native_ref_info: ObjectNativeRefInfo,
}

impl<'a> Deref for ObjectHandle<'a> {
    type Target = &'a Object;
    fn deref(&self) -> &&'a Object {
        &self.object
    }
}

pub struct ObjectNativeRefInfo {
    // TODO: Remove Rc
    n_refs: Rc<Cell<usize>>,

    // in case n_refs becomes zero
    gc_notified: bool,
}

impl ObjectInfo {
    pub fn new(obj: Box<Object>) -> ObjectInfo {
        ObjectInfo {
            object: obj,
            native_ref_info: ObjectNativeRefInfo {
                n_refs: Rc::new(Cell::new(0)),
                gc_notified: false,
            },
        }
    }

    pub fn gc_notify(&mut self) {
        self.native_ref_info.gc_notified = true;
    }

    pub fn as_object(&self) -> &Object {
        &*self.object
    }

    pub fn has_native_refs(&self) -> bool {
        if self.native_ref_info.n_refs.get() == 0 {
            false
        } else {
            true
        }
    }

    pub fn handle<'a>(&self) -> ObjectHandle<'a> {
        ObjectHandle {
            object: unsafe { ::std::mem::transmute::<&Object, &'static Object>(&*self.object) },
            _native_ref_info: self.native_ref_info.clone(),
        }
    }
}

impl Drop for ObjectInfo {
    fn drop(&mut self) {
        if self.native_ref_info.n_refs.get() != 0 {
            eprintln!("Attempting to drop object with alive references");
            ::std::process::abort();
        }
    }
}

impl Clone for ObjectNativeRefInfo {
    fn clone(&self) -> Self {
        self.n_refs.replace(self.n_refs.get() + 1);
        ObjectNativeRefInfo {
            n_refs: self.n_refs.clone(),
            gc_notified: false,
        }
    }
}

impl Drop for ObjectNativeRefInfo {
    fn drop(&mut self) {
        let n_refs = self.n_refs.get();

        if self.gc_notified {
            assert_eq!(n_refs, 0);
        } else {
            assert!(n_refs > 0);
            self.n_refs.replace(n_refs - 1);
        }
    }
}

pub struct TypedObjectHandle<'a, T: 'a> {
    _handle: ObjectHandle<'a>,
    value: &'a T,
}

impl<'a, T> Deref for TypedObjectHandle<'a, T>
where
    T: 'a,
{
    type Target = &'a T;
    fn deref(&self) -> &&'a T {
        &self.value
    }
}

impl<'a, T> TypedObjectHandle<'a, T>
where
    T: 'static,
{
    pub fn downcast_from(other: ObjectHandle<'a>) -> Option<TypedObjectHandle<'a, T>> {
        let value = match other.object.as_any().downcast_ref::<T>() {
            Some(v) => v,
            None => return None,
        };
        Some(TypedObjectHandle {
            _handle: other,
            value: value,
        })
    }
}

use std::collections::HashMap;

pub struct ObjectPool {
    pub objects: Vec<Option<ObjectInfo>>,
    object_idx_pool: Vec<usize>,
    pub global_objects: HashMap<String, Value>,

    alloc_count: usize,
}

impl ObjectPool {
    pub fn new() -> ObjectPool {
        ObjectPool {
            objects: vec![Some(ObjectInfo::new(Box::new(StaticRoot::new())))],
            object_idx_pool: vec![],
            global_objects: HashMap::new(),
            alloc_count: 0,
        }
    }

    /// Делает объект частью пула
    pub fn allocate(&mut self, mut inner: Box<Object>) -> usize {
        inner.initialize(self);

        let id = if let Some(id) = self.object_idx_pool.pop() {
            id
        } else {
            let objects = &mut self.objects;
            objects.push(None);
            objects.len() - 1
        };
        self.objects[id] = Some(ObjectInfo::new(inner));

        self.alloc_count += 1;

        id
    }

    fn deallocate(&mut self, id: usize) {
        let objects = &mut self.objects;
        let pool = &mut self.object_idx_pool;

        assert!(objects[id].is_some());

        objects[id] = None;
        pool.push(id);
    }

    /// Возвращает дескриптор объекта в `id`.
    ///
    /// Контроль может быть безопасно передан и
    /// базовый объект не будет собран сборщиком мусора
    /// до тех пор, пока все его дескрипторы  не будут освобождены(т.е использованы).
    ///
    /// Если пул объектов уничтожается до
    /// того как все дескрипторы будут уничтожены, процесс будет
    /// прерван из-за небезопасности работы памяти
    /// по ссылке.
    pub fn get<'a>(&self, id: usize) -> ObjectHandle<'a> {
        self.objects[id].as_ref().unwrap().handle()
    }

    /// Получает прямую ссылку на объект используя `id`.
    pub fn get_direct(&self, id: usize) -> &Object {
        self.objects[id].as_ref().unwrap().as_object()
    }

    /// Получает прямую типизированую ссылку на объект с `id`.
    /// Если тип не определить, возвращается None.
    pub fn get_direct_typed<T: 'static>(&self, id: usize) -> Option<&T> {
        self.get_direct(id).as_any().downcast_ref::<T>()
    }

    /// Получает прямую ссылку на объект с `id`.
    /// Если опустить ссылку нельзя то вы получаете ошибку `RuntimeError`.
    pub fn must_get_direct_typed<T: 'static>(&self, id: usize) -> &T {
        self.get_direct_typed(id)
            .unwrap_or_else(|| panic!("Type mismatch"))
    }

    /// Получает дескриптор типизированного объекта для объекта в `id`.
    /// Если downcast не работает, возвращается `None`.
    pub fn get_typed<'a, T: 'static>(&self, id: usize) -> Option<TypedObjectHandle<'a, T>> {
        TypedObjectHandle::downcast_from(self.get(id))
    }

    /// Получает дескриптор типизированного объекта для объекта в `id`.
    /// Если downcast не работает, это вызывает «RuntimeError».
    pub fn must_get_typed<'a, T: 'static>(&self, id: usize) -> TypedObjectHandle<'a, T> {
        self.get_typed(id)
            .unwrap_or_else(|| panic!("Type mismatch"))
    }

    pub fn get_static_root<'a>(&self) -> TypedObjectHandle<'a, StaticRoot> {
        self.get_typed(0).unwrap()
    }

    pub fn get_direct_static_root(&self) -> &StaticRoot {
        self.get_direct_typed(0).unwrap()
    }

    pub fn set_global_object<K: ToString>(&mut self, key: K, obj: Value) {
        let key = key.to_string();

        // Replacing static objects is denied to ensure
        // `get_global_object_ref` is safe.
        /*if self.global_objects.get(key.as_str()).is_some() {
            panic!("A static object with the same key already exists");
        }*/

        if let Value::Object(id) = obj {
            self.get_static_root().append_child(id);
        }
        self.global_objects.insert(key, obj);
    }

    pub fn get_global_object<K: AsRef<str>>(&self, key: K) -> Option<&Value> {
        let key = key.as_ref();
        self.global_objects.get(key)
    }

    pub fn get_alloc_count(&self) -> usize {
        self.alloc_count
    }

    pub fn reset_alloc_count(&mut self) {
        self.alloc_count = 0;
    }

    /// Запуск сборщика мусора с контекстом выполнения
    ///, предоставляемым данным стеком вызовов.
    pub fn collect(&mut self, stack: &CallStack) {
        let mut visited: Vec<bool> = vec![false; self.objects.len()];

        let mut dfs: Vec<usize> = Vec::new();
        dfs.push(0); // static root

        for id in stack.collect_objects() {
            dfs.push(id);
        }

        /*let objects: Vec<usize> = {
            use std::collections::HashSet;
            let mut objs = HashSet::new();
        
            for i in 0..stack.len() {
                let frame = &stack[i];
        
                if let Value::Object(id) = frame.this.get() {
                    objs.insert(id);
                }
        
                for i in 0..frame.arguments.len() {
                    let v = frame.arguments.get(i).unwrap();
                    if let Value::Object(id) = v {
                        objs.insert(*id);
                    }
                }
                for _i in 0..frame.locals.clone().len() {
                    for (_key,val) in frame.locals.iter() {
                        if let Value::Object(id) = val {
                            objs.insert(*id);
                        }
                    }
                }
        
                for i in 0..frame.stack.len() {
                    let v = frame.stack.get(i).unwrap();
        
                    if let Value::Object(id) = v {
                        objs.insert(*id);
                    }
                }
            }
            objs.into_iter().collect()
        };*/

        while !dfs.is_empty() {
            let id = dfs.pop().unwrap();

            if visited[id] {
                continue;
            }
            visited[id] = true;

            let obj = &self.objects[id].as_ref().unwrap();
            for child in obj.as_object().get_children() {
                dfs.push(child);
            }
        }

        for i in 0..visited.len() {
            if self.objects[i].is_some() && !visited[i] {
                if !self.objects[i].as_ref().unwrap().has_native_refs() {
                    self.objects[i].as_mut().unwrap().gc_notify();
                    self.deallocate(i);
                }
            }
        }
    }
}

impl Drop for ObjectPool {
    fn drop(&mut self) {
        for obj in &mut self.objects {
            if let Some(ref mut obj) = *obj {
                obj.gc_notify();
            }
        }
    }
}
