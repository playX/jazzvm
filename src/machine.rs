use super::frame::BasicBlock;
use super::frame::CallStack;
use super::frame::Frame;
use super::object::*;
use super::opcode::Opcode;
use super::value::Value;
use super::value::ValueContext;
use std::cmp::Ordering;
use std::ops::*;
use std::panic::{catch_unwind, resume_unwind, AssertUnwindSafe};

use super::jazz_std::array::Array;
use super::jazz_std::io::IO;

pub struct Machine {
    pub stack: CallStack,
    pub object_pool: ObjectPool,
}

impl Machine {
    pub fn new() -> Machine {
        let mut ret = Machine {
            stack: CallStack::new(4096 * 10),
            object_pool: ObjectPool::new(),
        };

        ret.create_global_object("IO", Box::new(IO::new()));
        ret.create_global_object("Array", Box::new(Array::new()));
        ret
    }

    #[inline]
    pub fn get_object_pool(&self) -> &ObjectPool {
        &self.object_pool
    }

    pub fn last_frame(&self) -> &Frame {
        return self.stack.top();
    }
    pub fn last_frame_mut(&mut self) -> &mut Frame {
        self.stack.top_mut()
    }

    pub fn create_global_object<K: ToString>(&mut self, key: K, obj: Box<Object>) {
        let obj_id = self.object_pool.allocate(obj);
        self.set_global_object(key, Value::Object(obj_id));
    }

    fn set_global_object<K: ToString>(&mut self, k: K, obj: Value) {
        self.object_pool.set_global_object(k, obj);
    }

    pub fn get_object_pool_mut(&mut self) -> &mut ObjectPool {
        &mut self.object_pool
    }

    pub fn push(&mut self, v: Value) {
        self.stack.top_mut().push(v);
    }

    pub fn gc(&mut self) {
        self.object_pool.collect(&self.stack);
    }

    pub fn invoke(
        &mut self,
        callable_val: Value,
        this: Value,
        field_name: Option<&str>,
        args: &[Value],
    ) {
        let callable_obj_id = match callable_val {
            Value::Object(id) => id,
            _ => {
                let frame = self.last_frame();
                let bb = &frame.basic_blocks[frame.current_basic_block_id];
                let code = &bb.code[bb.pc];
                panic!(format!(
                    "Not callable. Got: {:?}, current code: {:?}",
                    callable_val, code
                ))
            }
        };

        let callable_obj = self.object_pool.get(callable_obj_id);

        self.last_frame_mut().push(callable_val);
        self.stack.push();

        let thiss = self.last_frame().get_this();
        let ret = catch_unwind(AssertUnwindSafe(|| {
            let frame = self.stack.top_mut();

            frame.init_with_arguments(
                match this {
                    Value::Null => thiss,
                    _ => this,
                },
                args,
            );
            match field_name {
                Some(v) => callable_obj.call_field(v, self, Some(args.len())),
                None => callable_obj.call(self, Some(args.len())),
            }
        }));

        self.stack.pop();
        self.last_frame_mut().pop();
        
        match ret {
            Ok(v) => {
                self.last_frame_mut().push(v);
            }
            Err(e) => resume_unwind(e),
        }
    }

    pub fn get_global_object<K: AsRef<str>>(&mut self, key: K) -> Option<&Value> {
        self.object_pool.get_global_object(key)
    }

    pub fn execute_blocks(
        &mut self,
        bb: Vec<BasicBlock>,
        args: &[(Value, String)],
    ) -> Option<Value> {
        self.last_frame_mut().basic_blocks = bb;
        self.last_frame_mut().current_basic_block_id = 0;
        for argument in args.iter() {
            self.last_frame_mut()
                .locals
                .insert(argument.1.clone(), argument.0);
        }
        let value = loop {
            let result = self.execute_op();

            match result {
                Some(val) => break Some(val),
                None => {}
                //Some(Value::Null) => break None,
            }
        };

        value
    }

    pub fn execute_op(&mut self) -> Option<Value> {
        let op = self.stack.top_mut().fetch_opcode();
        //println!("{:?}",self.last_frame());
        macro_rules! pop {
            () => {
                self.stack.top_mut().pop()
            };
            ($value_variant: path) => {{
                match pop!() {
                    $value_variant(v) => v,
                    v => panic!(
                        "Expected to pop a value of type {}, but was {:?}",
                        stringify!($value_variant),
                        v
                    ),
                }
            }};
        }

        macro_rules! pop2 {
            () => {{
                (pop!(), pop!())
            }};
            ($value_variant: path) => {{
                match pop2!() {
                    ($value_variant(v), $value_variant(v2)) => (v, v2),
                    (v, v2) => panic!(
                        "Expected to pop a value of type {}, but was {:?} and {:?}",
                        stringify!($value_variant),
                        v,
                        v2
                    ),
                }
            }};
        }

        macro_rules! binop {
            ($value_variant: path, $binop: expr) => {{
                let v2 = pop!($value_variant);
                let v1 = pop!($value_variant);
                self.push($value_variant($binop(v1, v2)));
            }};
        }

        macro_rules! alloc {
            ($vm: expr,$v: expr) => {{
                let id = $vm.object_pool.allocate(Box::new($v));
                $vm.push(Value::Object(id));
            }};
        }

        match op {
            Opcode::LoadBool(b) => {
                self.push(Value::Bool(b));
                None
            }
            Opcode::LoadInt(i) => {
                let id = self.object_pool.allocate(Box::new(i));
                self.push(Value::Object(id));

                None
            }
            Opcode::LoadThis => {
                self.push(self.last_frame().this.get());
                None
            }
            Opcode::SetField(ref fname) => {
                let target = pop!();
                let obj = ValueContext::new(&target, &self.object_pool).as_object();
                obj.set_field(fname, pop!());
                None
            }
            Opcode::SetThis => {
                use std::cell::Cell;
                self.last_frame_mut().this = Cell::new(pop!());
                None
            }
            Opcode::LoadFloat(f) => {
                alloc!(self, f);

                None
            }
            Opcode::LoadDouble(d) => {
                alloc!(self, d);
                None
            }
            Opcode::LoadLong(l) => {
                alloc!(self, l);
                None
            }
            Opcode::LoadString(ref s) => {
                let obj = self.object_pool.allocate(Box::new(s.clone()));
                self.push(Value::Object(obj));
                None
            }

            Opcode::LoadObject(id) => {
                self.push(Value::Object(id));
                None
            }

            Opcode::Neg => {
                let v = pop!(Value::Object);
                self.invoke(Value::Object(v), Value::Null, Some("__neg__"), &[]);
                let v = pop!();
                self.push(v);
                None
            }
            Opcode::Not => {
                let v = pop!();
                match v {
                    Value::Object(obj) => {
                        self.invoke(Value::Object(obj), Value::Null, Some("__not__"), &[]);
                        let v = pop!();
                        self.push(v);
                    }
                    Value::Bool(b) => {
                        self.push(Value::Bool(!b));
                    }
                    _ => unimplemented!(),
                }
                None
            }
            Opcode::Add => {
                let (v1, v2) = pop2!();
                match (v2, v1) {
                    (Value::Object(id1), Value::Object(id2)) => {
                        let object =
                            ValueContext::new(&Value::Object(id2), &self.object_pool).as_object();
                        if object.typename() == "double" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f64();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__add__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else if object.typename() == "float" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f32();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__add__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else {
                            self.invoke(
                                Value::Object(id1),
                                Value::Object(id1),
                                Some("__add__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        }
                    }
                    (Value::Int(i), Value::Int(i2)) => self.push(Value::Int(i + i2)),
                    (Value::Float(f), Value::Int(i)) => self.push(Value::Float(f + i as f64)),
                    (Value::Int(i), Value::Float(f)) => self.push(Value::Float(i as f64 + f)),
                    (Value::Float(f), Value::Float(f2)) => self.push(Value::Float(f + f2)),
                    v => panic!("Cannot apply Opcode::Add to {:?}", v),
                }
                None
            }
            Opcode::Sub => {
                let (v1, v2) = pop2!();
                match (v2, v1) {
                    (Value::Object(id1), Value::Object(id2)) => {
                        let object =
                            ValueContext::new(&Value::Object(id2), &self.object_pool).as_object();
                        if object.typename() == "double" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f64();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__sub__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else if object.typename() == "float" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f32();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__sub__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else {
                            self.invoke(
                                Value::Object(id1),
                                Value::Object(id1),
                                Some("__sub__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        }
                    }
                    (Value::Int(i), Value::Int(i2)) => self.push(Value::Int(i - i2)),
                    (Value::Float(f), Value::Int(i)) => self.push(Value::Float(f - i as f64)),
                    (Value::Int(i), Value::Float(f)) => self.push(Value::Float(i as f64 - f)),
                    (Value::Float(f), Value::Float(f2)) => self.push(Value::Float(f - f2)),
                    v => panic!("Cannot apply Opcode::Sub to {:?}", v),
                }
                None
            }

            Opcode::Div => {
                let (v1, v2) = pop2!();
                match (v2, v1) {
                    (Value::Object(id1), Value::Object(id2)) => {
                        let object =
                            ValueContext::new(&Value::Object(id2), &self.object_pool).as_object();
                        if object.typename() == "double" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f64();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__div__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else if object.typename() == "float" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f32();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__div__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else {
                            self.invoke(
                                Value::Object(id1),
                                Value::Object(id1),
                                Some("__div__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        }
                    }
                    (Value::Int(i), Value::Int(i2)) => self.push(Value::Int(i / i2)),
                    (Value::Float(f), Value::Int(i)) => self.push(Value::Float(f / i as f64)),
                    (Value::Int(i), Value::Float(f)) => self.push(Value::Float(i as f64 / f)),
                    (Value::Float(f), Value::Float(f2)) => self.push(Value::Float(f / f2)),
                    v => panic!("Cannot apply Opcode::Div to {:?}", v),
                }
                None
            }

            Opcode::Mul => {
                let (v1, v2) = pop2!();
                match (v2, v1) {
                    (Value::Object(id1), Value::Object(id2)) => {
                        let object =
                            ValueContext::new(&Value::Object(id2), &self.object_pool).as_object();
                        if object.typename() == "double" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f64();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__mul__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else if object.typename() == "float" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f32();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__mul__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else {
                            self.invoke(
                                Value::Object(id1),
                                Value::Object(id1),
                                Some("__mul__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        }
                    }
                    (Value::Int(i), Value::Int(i2)) => self.push(Value::Int(i * i2)),
                    (Value::Float(f), Value::Int(i)) => self.push(Value::Float(f * i as f64)),
                    (Value::Int(i), Value::Float(f)) => self.push(Value::Float(i as f64 * f)),
                    (Value::Float(f), Value::Float(f2)) => self.push(Value::Float(f * f2)),
                    v => panic!("Cannot apply Opcode::Mul to {:?}", v),
                }
                None
            }

            Opcode::Mod => {
                let (v1, v2) = pop2!();
                match (v2, v1) {
                    (Value::Object(id1), Value::Object(id2)) => {
                        let object =
                            ValueContext::new(&Value::Object(id2), &self.object_pool).as_object();
                        if object.typename() == "double" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f64();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__mod__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else if object.typename() == "float" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f32();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__mod__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else {
                            self.invoke(
                                Value::Object(id1),
                                Value::Object(id1),
                                Some("__mod__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        }
                    }
                    (Value::Int(i), Value::Int(i2)) => self.push(Value::Int(i % i2)),
                    (Value::Float(f), Value::Int(i)) => self.push(Value::Float(f % i as f64)),
                    (Value::Int(i), Value::Float(f)) => self.push(Value::Float(i as f64 % f)),
                    (Value::Float(f), Value::Float(f2)) => self.push(Value::Float(f % f2)),
                    v => panic!("Cannot apply Opcode::Mod to {:?}", v),
                }
                None
            }

            Opcode::Pow => {
                let (v1, v2) = pop2!();
                match (v2, v1) {
                    (Value::Object(id1), Value::Object(id2)) => {
                        let object =
                            ValueContext::new(&Value::Object(id2), &self.object_pool).as_object();
                        if object.typename() == "double" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f64();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__pow__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else if object.typename() == "float" {
                            let object1 = ValueContext::new(&Value::Object(id1), &self.object_pool)
                                .as_object()
                                .to_f32();
                            let object1 = self.object_pool.allocate(Box::new(object1));
                            self.invoke(
                                Value::Object(object1),
                                Value::Object(object1),
                                Some("__pow__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        } else {
                            self.invoke(
                                Value::Object(id1),
                                Value::Object(id1),
                                Some("__pow__"),
                                &[Value::Object(id2)],
                            );
                            let v = pop!();
                            self.push(v);
                        }
                    }
                    (Value::Int(i), Value::Int(i2)) => self.push(Value::Int(i.pow(i2 as u32))),
                    (Value::Float(f), Value::Int(i)) => self.push(Value::Float(f.powi(i as i32))),
                    (Value::Int(i), Value::Float(f)) => self.push(Value::Float((i as f64).powf(f))),
                    (Value::Float(f), Value::Float(f2)) => self.push(Value::Float(f.powf(f2))),
                    v => panic!("Cannot apply Opcode::Pow to {:?}", v),
                }
                None
            }

            Opcode::LoadNull => {
                self.push(Value::Null);
                None
            }
            Opcode::GotoIfTrue(idx) => {
                let boolean = pop!(Value::Bool);
                if boolean {
                    self.stack.top_mut().goto(idx);
                }
                None
            }
            Opcode::GotoIfFalse(idx) => {
                let boolean = pop!(Value::Bool);
                if !boolean {
                    self.stack.top_mut().goto(idx);
                }
                None
            }
            Opcode::Goto(idx) => {
                self.stack.top_mut().goto(idx);
                None
            }

            Opcode::BasicBlockGoto(idx) => {
                self.stack.top_mut().switch_basic_block(idx);
                None
            }
            Opcode::BasicBlockIfFalse(idx) => {
                let b = pop!(Value::Bool);
                if !b {
                    self.stack.top_mut().switch_basic_block(idx)
                }
                None
            }
            Opcode::BasicBlockIfTrue(idx) => {
                let b = pop!(Value::Bool);
                if b {
                    self.stack.top_mut().switch_basic_block(idx)
                }
                None
            }

            Opcode::GetGlobal(ref key) => {
                //let pool = &self.object_pool;
                let maybe_target_obj = self.get_global_object(key).map(|v| *v);

                if let Some(target_obj) = maybe_target_obj {
                    self.push(target_obj);
                } else {
                    self.push(Value::Null);
                }

                None
            }

            Opcode::SetGlobal(ref key) => {
                let value = self.last_frame_mut().pop();
                self.object_pool.set_global_object(key, value);
                None
            }

            Opcode::GetLocal(ref name) => {
                self.push(
                    *self
                        .stack
                        .top()
                        .locals
                        .get(name)
                        .expect(&format!("no value with name: `{}`", name)),
                );
                None
            }
            Opcode::SetLocal(ref name) => {
                let val = pop!();
                match val {
                    Value::Object(idx) => {
                        let clone = ValueContext::new(&Value::Object(idx), &self.object_pool)
                            .as_object()
                            .o_clone(&mut self.object_pool);
                        self.stack.top_mut().locals.insert(name.clone(), clone);
                    }
                    v => {
                        self.stack.top_mut().locals.insert(name.clone(),v.clone());
                    }
                }

                None
            }



            Opcode::Eq => {
                /*let (v1,v2) = pop2!();
                
                match (v2,v1) {
                    (Value::Int(i),Value::Int(i2)) => self.push(Value::Bool(i == i2)),
                    (Value::Float(f1),Value::Float(f2)) => self.push(Value::Bool(f1 == f2)),
                    (Value::Bool(b1),Value::Bool(b2)) => self.push(Value::Bool(b1 == b2)),
                    _ => unimplemented!(),
                }*/

                let pool = &self.object_pool;
                let ord = {
                    let (left, right) = pop2!();

                    let left_ctx = ValueContext::new(&left, pool);
                    let right_ctx = ValueContext::new(&right, pool);

                    left_ctx.compare(&right_ctx)
                };

                self.push(Value::Bool(ord == Some(Ordering::Equal)));

                None
            }
            Opcode::Ge => {
                let pool = &self.object_pool;
                let ord = {
                    let (left, right) = pop2!();

                    let left_ctx = ValueContext::new(&left, pool);
                    let right_ctx = ValueContext::new(&right, pool);

                    left_ctx.compare(&right_ctx)
                };

                self.push(Value::Bool(
                    ord == Some(Ordering::Greater) || ord == Some(Ordering::Equal),
                ));
                None
            }

            Opcode::Gt => {
                let pool = &self.object_pool;
                let ord = {
                    let (left, right) = pop2!();

                    let left_ctx = ValueContext::new(&left, pool);
                    let right_ctx = ValueContext::new(&right, pool);

                    left_ctx.compare(&right_ctx)
                };

                self.push(Value::Bool(ord == Some(Ordering::Greater)));
                None
            }
            Opcode::Lt => {
                let pool = &self.object_pool;
                let ord = {
                    let (left, right) = pop2!();

                    let left_ctx = ValueContext::new(&left, pool);
                    let right_ctx = ValueContext::new(&right, pool);

                    left_ctx.compare(&right_ctx)
                };

                self.push(Value::Bool(ord == Some(Ordering::Less)));
                None
            }
            Opcode::Le => {
                let pool = &self.object_pool;
                let ord = {
                    let (left, right) = pop2!();

                    let left_ctx = ValueContext::new(&left, pool);
                    let right_ctx = ValueContext::new(&right, pool);

                    left_ctx.compare(&right_ctx)
                };

                self.push(Value::Bool(
                    ord == Some(Ordering::Less) || ord == Some(Ordering::Equal),
                ));
                None
            }

            Opcode::Return => {
                return Some(pop!());
            }

            Opcode::And => {
                let (v1, v2) = pop2!(Value::Bool);
                self.push(Value::Bool(v1 && v2));
                None
            }
            Opcode::Or => {
                let (v1, v2) = pop2!(Value::Bool);
                self.push(Value::Bool(v1 || v2));
                None
            }
            Opcode::Reverse(count) => {
                let mut values = Vec::with_capacity(count);

                for _ in 0..count {
                    values.push(pop!());
                }
                values.reverse();
                for i in 0..count {
                    self.push(values[i]);
                }

                None
            }

            Opcode::GetField(ref field_name) => {
                let target = pop!();

                let object = ValueContext::new(&target, self.get_object_pool()).as_object_direct();
                self.push(
                    object
                        .get_field(&self.object_pool, field_name)
                        .expect(&format!("field not found `{}`", field_name)),
                );
                None
            }

            Opcode::Dup => {
                let val = self.last_frame().stack.last().unwrap();
                self.push(*val);
                None
            }


            Opcode::CallField(argc) => {
                let (target, this, field_name, args) = {
                    let target = pop!();
                    let this = pop!();
                    let field_name = pop!();
                    let mut args: Vec<Value> = Vec::with_capacity(argc);
                    for _ in 0..argc {
                        args.push(pop!());
                    }
                    (target, this, field_name, args)
                };
                let field_name = ValueContext::new(&field_name, self.get_object_pool())
                    .to_str()
                    .to_string();

                self.invoke(target, this, Some(field_name.as_str()), args.as_slice());
                None
            }

            Opcode::Call(argc) => {
                let (target, this, args) = {
                    let frame = self.last_frame_mut();
                    let target = frame.pop();
                    let this = frame.pop();

                    let mut args: Vec<Value> = Vec::with_capacity(argc);
                    for _ in 0..argc {
                        args.push(frame.pop());
                    }
                    (target, this, args)
                };

                self.invoke(target, this, None, &args);
                None
            }

            Opcode::GetArgument(id) => {
                let arg = self.last_frame().must_get_argument(id);
                self.push(*arg);
                None
            }
            Opcode::GetNArguments => {
                let len = self.last_frame().arguments.len();
                self.push(Value::Int(len as i64));
                None
            }

            op => panic!("Unimplemented {:?}", op),
        }
    }
}
