use super::frame::BasicBlock;
use super::*;
use std::any::*;
use std::cell::*;
pub enum Function {
    Virtual(RefCell<VirtualFunction>),
    Native(NativeFunction),
}

pub struct VirtualFunction {
    basic_blocks: Vec<BasicBlock>,
    rt_handles: Vec<usize>,
    should_optimize: bool,
    this: Option<Value>,
    pub args: Vec<String>,
}

#[derive(Clone, Debug)]
pub struct VirtualFunctionInfo {
    pub basic_blocks: Vec<BasicBlock>,
}

pub type NativeFunction = Box<Fn(&mut Machine, Vec<Value>) -> Value + Send>;

impl Object for Function {
    fn initialize(&mut self, pool: &mut ObjectPool) {
        self.static_optimize(pool);
    }

    fn o_clone(&self, _p: &mut ObjectPool) -> Value {
        panic!("Cannot clone!")
    }

    fn get_children(&self) -> Vec<usize> {
        match *self {
            Function::Virtual(ref f) => f.borrow().rt_handles.clone(),
            Function::Native(_) => Vec::new(),
        }
    }

    fn as_any(&self) -> &Any {
        self as &Any
    }

    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        match field_name {
            "disassemble" => {
                let mut string = String::new();
                match *self {
                    Function::Virtual(ref vf) => {
                        let vf = vf.borrow();
                        for bb in vf.basic_blocks.iter() {
                            for i in 0..bb.code.len() {
                                string.push_str(&format!("{}: {:?}\n", i, bb.code[i]));
                            }
                        }
                    }
                    _ => panic!("Cannot disassemble native function"),
                }
                let id = executor.object_pool.allocate(Box::new(string));
                return Value::Object(id);
            }
            _ => panic!("Field not found"),
        }
    }

    fn call(&self, executor: &mut Machine, argc: Option<usize>) -> Value {
        match *self {
            Function::Virtual(ref vf) => {
                let vf = vf.borrow();
                if let Some(this) = vf.this {
                    executor.last_frame_mut().set_this(this);
                }
                let args = {
                    let mut temp: Vec<(Value, String)> = vec![];
                    /*for _ in 0..vf.args.len() {
                        temp.push(executor.last_frame_mut().pop());
                    }*/

                    let mut i = 0;
                    while i < vf.args.len() {
                        let v = executor
                            .last_frame_mut()
                            .arguments
                            .pop()
                            .expect("no value to pop in args");
                        let str = &vf.args[i];
                        temp.push((v, str.clone()));
                        i += 1;
                    }
                    temp
                };
                //executor.eval_basic_basic_blocks(vf.basic_blocks.as_slice(), 0)
                executor
                    .execute_blocks(vf.basic_blocks.clone(), &args)
                    .unwrap()
            }
            Function::Native(ref nf) => {
                let args = {
                    let mut temp = vec![];
                    let mut i = 0;
                    if argc.is_some() {
                        while i < argc.unwrap() {
                            let v = executor
                                .last_frame_mut()
                                .arguments
                                .pop()
                                .expect("no value to pop in arguments");
                            temp.push(v);
                            i += 1;
                        }
                    }
                    temp
                };

                nf(executor, args)
            }
        }
    }
}

impl Function {
    pub fn from_basic_blocks(basic_blocks: Vec<BasicBlock>, args: Vec<String>) -> Function {
        let vf = VirtualFunction {
            basic_blocks: basic_blocks,
            rt_handles: Vec::new(),
            should_optimize: false,
            this: None,
            args: args,
        };

        Function::Virtual(RefCell::new(vf))
    }

    pub fn bind_this(&self, this: Value) {
        if let Function::Virtual(ref f) = *self {
            if let Ok(mut f) = f.try_borrow_mut() {
                if f.this.is_some() {
                    panic!("Cannot rebind this");
                }
                f.this = Some(this);
                if let Value::Object(id) = this {
                    f.rt_handles.push(id);
                }
            } else {
                panic!("Cannot bind from inside the function");
            }
        } else {
            panic!("Binding this is only supported on virtual functions");
        }
    }

    pub fn enable_optimization(&mut self) {
        if let Function::Virtual(ref mut f) = *self {
            f.borrow_mut().should_optimize = true;
        }
    }

    pub fn from_native(nf: NativeFunction) -> Function {
        Function::Native(nf)
    }

    pub fn to_virtual_info(&self) -> Option<VirtualFunctionInfo> {
        match *self {
            Function::Virtual(ref vf) => Some(VirtualFunctionInfo {
                basic_blocks: vf.borrow().basic_blocks.clone(),
            }),
            Function::Native(_) => None,
        }
    }

    pub fn from_virtual_info(vinfo: VirtualFunctionInfo) -> Self {
        Function::from_basic_blocks(vinfo.basic_blocks, vec![])
    }

    pub fn static_optimize(&self, _pool: &mut ObjectPool) {
        if let Function::Virtual(ref f) = *self {
            if let Ok(f) = f.try_borrow_mut() {
                if f.should_optimize {
                    //f.static_optimize(pool);
                }
            } else {
                panic!("Cannot optimize virtual functions within itself");
            }
        }
    }

    pub fn dynamic_optimize(&self, _pool: &mut ObjectPool) {
        if let Function::Virtual(ref f) = *self {
            if let Ok(f) = f.try_borrow_mut() {
                if f.should_optimize {
                    //f.dynamic_optimize(pool);
                }
            } else {
                panic!("Cannot optimize virtual functions within itself");
            }
        }
    }
}

impl VirtualFunction {}

use super::object::ObjectPool;
use super::value::Value;

/*pub struct FunctionOptimizer<'a> {
    binded_this: Option<Value>,
    basic_blocks: &'a mut Vec<BasicBlock>,
    rt_handles: &'a mut Vec<usize>,
    pool: &'a mut ObjectPool
}

impl<'a> FunctionOptimizer<'a> {
    pub fn new(basic_blocks: &'a mut Vec<BasicBlock>, rt_handles: &'a mut Vec<usize>, pool: &'a mut ObjectPool) -> FunctionOptimizer<'a> {
        FunctionOptimizer {
            binded_this: None,
            basic_blocks: basic_blocks,
            rt_handles: rt_handles,
            pool: pool
        }
    }

    pub fn set_binded_this(&mut self, this: Option<Value>) {
        self.binded_this = this;
    }

    pub fn static_optimize(&mut self) {
        for _ in 0..3 {
            self.transform_const_locals();

            // Run optimizations on each basic BasicBlock
            for bb in self.basic_blocks.iter_mut() {
                // LoadString -> LoadObject
                bb.transform_const_string_loads(self.rt_handles, self.pool);

                // (LoadObject, GetGlobal) -> LoadValue
                bb.transform_const_static_loads(self.rt_handles, self.pool);

                while bb.transform_const_get_fields(self.rt_handles, self.pool, self.binded_this) {
                    bb.flatten_stack_maps();
                    bb.remove_nops();
                }
                bb.transform_const_calls();
                bb.remove_nops();
            }
        }

        // These should only run once
        for bb in self.basic_blocks.iter_mut() {
            bb.build_bulk_loads();
            bb.rebuild_stack_patterns();
        }

        self.simplify_cfg();
    }

    pub fn dynamic_optimize(&mut self) {
        for bb in self.basic_blocks.iter_mut() {
            // LoadString -> LoadObject
            bb.transform_const_string_loads(self.rt_handles, self.pool);

            // (LoadObject, GetGlobal) -> LoadValue
            bb.transform_const_static_loads(self.rt_handles, self.pool);

            while bb.transform_const_get_fields(self.rt_handles, self.pool, self.binded_this) {
                bb.flatten_stack_maps();
                bb.remove_nops();
            }
            bb.transform_const_calls();
            bb.remove_nops();

            bb.build_bulk_loads();
        }
    }

    pub fn transform_const_locals(&mut self) {
        for bb in self.basic_blocks.iter_mut() {
            bb.transform_const_BasicBlock_locals();
        }
    }

    pub fn simplify_cfg(&mut self) {
        if self.basic_blocks.len() == 0 {
            return;
        }

        let n_basic_basic_blocks: usize = self.basic_blocks.len();

        let mut out_edges: Vec<HashSet<usize>> = vec! [ HashSet::new(); n_basic_basic_blocks ];
        let mut in_edges: Vec<HashSet<usize>> = vec! [ HashSet::new(); n_basic_basic_blocks ];
        for i in 0..n_basic_basic_blocks {
            let (a, b) = self.basic_blocks[i].branch_targets();
            if let Some(v) = a {
                out_edges[i].insert(v);
                in_edges[v].insert(i);
            }
            if let Some(v) = b {
                out_edges[i].insert(v);
                in_edges[v].insert(i);
            }
        }

        for i in 0..n_basic_basic_blocks {
            if out_edges[i].len() == 1 {
                let j = *out_edges[i].iter().nth(0).unwrap();
                if in_edges[j].len() == 1 {
                    if *in_edges[j].iter().nth(0).unwrap() == i {
                        //debug!("[simplify_cfg] Found unique connection: {} <-> {}", i, j);
                        out_edges.swap(i, j);
                        out_edges[j].clear();
                        in_edges[j].clear();
                        let v = ::std::mem::replace(
                            &mut self.basic_blocks[j],
                            BasicBlock::from_opcodes(Vec::new())
                        );
                        self.basic_blocks[i].join(v);
                    }
                }
            }
        }

        let mut dfs_stack: Vec<usize> = Vec::new();
        let mut dfs_visited: Vec<bool> = vec![ false; n_basic_basic_blocks ];

        dfs_visited[0] = true;
        dfs_stack.push(0);

        while !dfs_stack.is_empty() {
            let current = dfs_stack.pop().unwrap();

            for other in &out_edges[current] {
                if !dfs_visited[*other] {
                    dfs_visited[*other] = true;
                    dfs_stack.push(*other);
                }
            }
        }

        // collect unused basic_blocks
        {
            let unused_basic_blocks: BTreeSet<usize> = (0..self.basic_blocks.len()).filter(|i| !dfs_visited[*i]).collect();
            let mut tail = n_basic_basic_blocks - 1;
            let mut remap_list: Vec<(usize, usize)> = Vec::new(); // (to, from)
            for id in &unused_basic_blocks {
                while tail > *id {
                    if unused_basic_blocks.contains(&tail) {
                        tail -= 1;
                    } else {
                        break;
                    }
                }

                // Implies tail > 0
                if tail <= *id {
                    break;
                }

                // Now `id` is the first unused BasicBlock and `tail`
                // is the last used BasicBlock
                // Let's exchange them
                remap_list.push((*id, tail));
                self.basic_blocks.swap(*id, tail);
                tail -= 1;
            }
            while self.basic_blocks.len() > tail + 1 {
                self.basic_blocks.pop().unwrap();
            }
            for (to, from) in remap_list {
                for bb in self.basic_blocks.iter_mut() {
                    let replaced = bb.try_replace_branch_targets(to, from);
                    if replaced {
                        //debug!("[simplify_cfg] Branch target replaced: {} -> {}", from, to);
                    }
                }
            }
            //n_basic_basic_blocks = self.basic_blocks.len();
        }
    }
}*/
